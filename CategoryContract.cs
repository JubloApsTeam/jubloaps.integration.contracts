﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace JubloAps.Integration.Contracts
{
    public class CategoryContract
    {
        [RegularExpression("^[0-9]*$", ErrorMessage = "Invalid Category")]
        public string Name { get; set; }
    }
}
