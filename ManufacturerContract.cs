﻿using System;
using System.ComponentModel.DataAnnotations;

namespace JubloAps.Integration.Contracts
{
    public class ManufacturerContract
    {
        [Required]
        public string Key { get; set; }
        [Required]
        public int Index { get; set; }
        [Required]
        public string Name { get; set; }
    }
}
