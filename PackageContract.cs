﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace JubloAps.Integration.Contracts
{
    public class PackageContract
    {
        [Range(1, int.MaxValue, ErrorMessage = "EntityId must be between {1} and {2}")]
        public string EntityId { get; set; }
        [Required]
        public string Action { get; set; }
        [Required]
        public int ManufacturerId { get; set; }
        [Required]
        public string Disclaimer { get; set; }
        [Required]
        public string Name { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "EntityId must be minimum 0,5")]
        public int InstallationTime { get; set; }
        [Required]
        public bool? IsActive { get; set; }
        [Required]
        public string ResourcenId { get; set; }
        [Required]
        public string ResourceVersion { get; set; }
        [Required]
        public List<CategoryContract> Category { get; set; }
        [Required]
        public List<PackageUnitContract> Units { get; set; }
        [Required]
        public string Description { get; set; }
    }
}
