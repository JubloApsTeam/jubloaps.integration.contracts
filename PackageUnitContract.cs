﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace JubloAps.Integration.Contracts
{
    public class PackageUnitContract
    {
        [Range(1, int.MaxValue, ErrorMessage = "EntityId must be between {1} and {2}")]
        public int EntityId { get; set; }
        [Required]
        public string UniqueValue { get; set; }
    }
}
