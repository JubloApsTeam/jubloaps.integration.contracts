﻿using System.ComponentModel.DataAnnotations;

namespace JubloAps.Integration.Contracts
{
    public class ProductContract
    {
        [Required]
        public string ProductNumber { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public double Price { get; set; }
        [Required]
        public double ListPrice { get; set; }
        public string Manufacturer { get; set; }
        public string ImageUrl { get; set; }
}
}