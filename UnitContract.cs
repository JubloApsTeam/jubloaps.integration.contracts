﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace JubloAps.Integration.Contracts
{
    public class UnitContract
    {
        [Required]
        public string EntityId { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "EntityId must be between {1} and {2}")]
        public int ItemAmount { get; set; }
        [Required]
        public bool IsPrimaryItem { get; set; }
        [Required]
        public int ItemPriority { get; set; }
        [Required]
        public string Action { get; set; }
    }
}
