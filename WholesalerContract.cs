﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace JubloAps.Integration.Contracts
{
    public class WholesalerContract
    {
        [Required]
        public int WholesalerId { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public List<ProductContract> Product  { get; set; } = new List<ProductContract>();

}
}
